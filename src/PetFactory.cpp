#include "PetFactory.hpp"

namespace myGame
{
	std::shared_ptr<Pet> createPet(std::string name)
	{
		std::shared_ptr<Pet> pet = nullptr;
		if (name == "Cat")
			pet = std::make_shared<Cat>(200, 800);
		//else if (name == "Dog")
		//	pet = new Dog(200, 800);

		if (!pet->LoadTextures())
			return nullptr;
		if (!pet->LoadSounds())
			return nullptr;

		return pet;
	}
}