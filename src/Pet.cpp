#include "Pet.hpp"

namespace myGame
{

	Pet::Pet(int x, int y)
	{
		m_x = x;
		m_y = y;
	}
	Pet::~Pet() {}

	sf::Sprite Pet::Get()
	{
		return m_spritePet;
	}

	void Pet::Say()
	{
		m_sound.play();
	}

	void Pet::Move(Direction dir)
	{
		if (dir == Direction::Right)
		{
			m_x += m_velocity;
			if (m_frameNumber == 0)
			{
				m_frameNumber = 1;
				m_spritePet.setTexture(m_texturePetRight);
			}
			else
			{
				m_frameNumber = 0;
				m_spritePet.setTexture(m_texturePetRightNext);
			}
		}
		else if (dir == Direction::Left)
		{
			m_x -= m_velocity;
			if (m_frameNumber == 0)
			{
				m_frameNumber = 1;
				m_spritePet.setTexture(m_texturePetLeft);
			}
			else
			{
				m_frameNumber = 0;
				m_spritePet.setTexture(m_texturePetLeftNext);
			}
		}
	}

	void Pet::Jump()
	{
		m_vy = -m_velocity;
	}

	void Pet::UpdateObstacles(std::vector<std::shared_ptr<Obstacle>>& obstacles)
	{
		if (m_vy < 10)
			m_vy++;

		m_y += m_vy;

		// �������� �� ����� �������� �� ������� �������� ����
		if (m_x <= obstacles[0]->leftX())
			m_x = obstacles[0]->leftX();
		if (m_x >= obstacles[0]->rightX())
			m_x = obstacles[0]->rightX();

		m_isOnFloor = false;
		for (const auto& obstacle : obstacles)
		{
			if (m_x >= obstacle->leftX() && m_x <= obstacle->rightX()
				&& m_y > obstacle->Y() && abs(m_y - obstacle->Y()) < 20)
			{
				m_y = obstacle->Y();
				m_isOnFloor = true;
			}
		}

		m_spritePet.setPosition(m_x, m_y - shiftY());
	}

	int Pet::UpdateObjects(std::vector<std::shared_ptr<Object>>& objects)
	{
		int touchedObjects = 0;
		for (int i=0;i<objects.size();i++)
		{
			const auto& object = objects[i];

			int oX = object->X();
			int oY = object->Y();
			int oR = object->R();

			int distR = m_r + oR;
			int distXY = (m_x - oX) * (m_x - oX) + (m_y - oY) * (m_y - oY);

			// ��������� �������
			if (distR * distR > distXY)
			{
				objects.erase(objects.begin() + i);
				touchedObjects++;
			}
		}
		return touchedObjects;
	}
}