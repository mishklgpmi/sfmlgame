#include "Game.hpp"

namespace myGame
{
    Game::Game(std::shared_ptr<sf::RenderWindow> window)
    {
        m_window = window;// std::make_shared<sf::RenderWindow>(sf::VideoMode(m_width, m_height), "Destroyeer!");
        m_width = m_window->getSize().x;
        m_height = m_window->getSize().y;
    }
    Game::~Game()
    {
    }

    bool Game::setMusic()
    {
        if (!m_music.openFromFile("music\\back.ogg"))
            return false; // error
        sf::Music::TimeSpan span(sf::seconds(0), sf::seconds(4.4));
        m_music.setLoopPoints(span);
        m_music.setLoop(true);
        m_music.setVolume(20);
        m_music.play();
        return true;
    }

    bool Game::setBackground()
    {
        if (!m_background.loadFromFile("assets\\background.jpg"))
        {
            std::cerr << "Loading background error." << std::endl;
            return false;
        }

        m_backgroundSprite.setTexture(m_background);
        return true;
    }

    bool Game::setMainHero(std::string name)
    {
        m_pet = myGame::createPet(name);
        if (m_pet == nullptr)
        {
            return false;
        }
        return true;
    }

    bool Game::Setup()
    {
        if (!setMusic())
            return false;
        if (!setBackground())
            return false;
        if (!setInfo())
            return false;
        if (!setMainHero("Cat"))
            return false;
        if (!setObstacles())
            return false;
        if (!setObjects())
            return false;
        if (!setDestroedObjects())
            return false;
        return true;
    }

    void Game::LifeCycle()
    {
        while (m_window->isOpen())
        {
            sf::Event event;
            while (m_window->pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                {
                    m_music.stop();
                    return;
                }
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
            {
                m_pet->Say();
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
            {
                if(m_pet->CanJump())
                    m_pet->Jump();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            {
                m_pet->Move(myGame::Direction::Right);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                m_pet->Move(myGame::Direction::Left);
            }

            // ��������� �������������� �������� � ��������� ����
            m_pet->UpdateObstacles(m_obstacles);
            m_score += m_pet->UpdateObjects(m_objects);
            m_scoreText.setString(std::to_string(m_score));

            m_pet->UpdateDestroedObjects(m_destroyedObjects);

            // ���������� ��� ��������
            Gravity();

            m_window->clear();
            m_window->draw(m_backgroundSprite);
            m_window->draw(m_scoreText);

            // ����� �����������
            for (const auto& obstacle : m_obstacles)
                m_window->draw(*obstacle->Get());

            // ����� ��������
            for (const auto& object : m_objects)
                m_window->draw(*object->Get());

            // ����� ������������ ��������
            for (const auto& destroedObject : m_destroyedObjects)
                m_window->draw(*destroedObject->Get());

            m_window->draw(m_pet->Get());
            m_window->display();

            std::this_thread::sleep_for(std::chrono::milliseconds(16));
        }
    }
}