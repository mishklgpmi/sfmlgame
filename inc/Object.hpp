#pragma once
#include <memory>

namespace myGame
{

	class Object
	{
	public:
		Object(int x, int y) 
		{
			m_x = x;
			m_y = y;
			m_r = 50;

			m_shape = std::make_shared<sf::Sprite>();
			//m_shape->setFillColor(sf::Color::Red);
			m_shape->setPosition(m_x, m_y);
			m_shape->setOrigin(50, 50);
			m_shape->setScale(0.0933706816059757, 0.3289473684210526);

			m_shapeTexture.loadFromFile("assets\\sausage.png");

			m_shape->setTexture(m_shapeTexture);
		}
		std::shared_ptr<sf::Sprite> Get()
		{
			return m_shape;
		}

		int R() { return m_r; }
		int X() { return m_x; }
		int Y() { return m_y; }

		virtual ~Object() {}
	protected:
		int m_x, m_y, m_r;

		sf::Texture m_shapeTexture;
		std::shared_ptr<sf::Sprite> m_shape;
	};

	class Sausage : public Object
	{
	public:
		Sausage(int x, int y) : Object(x, y)
		{
			
		}
		virtual ~Sausage() {}

	private:
		
	};

}
