#pragma once
#include "Pet.hpp"
#include "Cat.hpp"

namespace myGame
{

	std::shared_ptr<Pet> createPet(std::string name);

}
