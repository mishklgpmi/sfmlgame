#pragma once
#include <memory>

namespace myGame
{
	class DestroyedObjects
	{
	public:
		DestroyedObjects(int x, int y)
		{
			m_x = x;
			m_y = y;
			m_r = 50;

			m_x = x;
			m_y = y;
			m_r = 50;

			m_shape = std::make_shared<sf::Sprite>();
			//m_shape->setFillColor(sf::Color::Red);

			m_shapeTexture.loadFromFile("assets\\box.png");
			m_shapeFireTexture.loadFromFile("assets\\box_fire.png");

			m_shape->setTexture(m_shapeTexture);
			m_shape->setPosition(m_x, m_y - m_shape->getTexture()->getSize().y / 2);
			m_shape->setOrigin(50, 50);
		}
		~DestroyedObjects() {}

		std::shared_ptr<sf::Sprite> Get()
		{
			return m_shape;
		}

		int R() { return m_r; }
		int X() { return m_x; }
		int Y() { return m_y; }

		void dX(int x) 
		{ 
			m_x += x; 
			m_shape->setPosition(m_x, m_y);
		}
		void dY(int y) 
		{
			m_y += y; 
			m_shape->setPosition(m_x, m_y - m_shape->getTexture()->getSize().y / 2);
		}

		void X(int x) 
		{ 
			m_x = x; 
			m_shape->setPosition(m_x, m_y);
		}
		void Y(int y) 
		{ 
			m_y = y;
			m_shape->setPosition(m_x, m_y - m_shape->getTexture()->getSize().y / 2);
		}

		void Rotate()
		{
			m_shape->rotate(10);
		}

		void Explosion()
		{
			m_shape->setTexture(m_shapeFireTexture);
		}

	private:
		int m_x, m_y, m_r;

		sf::Texture m_shapeTexture;
		sf::Texture m_shapeFireTexture;
		std::shared_ptr<sf::Sprite> m_shape;
	};
}
