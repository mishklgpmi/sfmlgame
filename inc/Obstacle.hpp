#pragma once

namespace myGame
{

	class Obstacle
	{
	public:
		Obstacle(int y, int leftX, int rightX) 
		{
			m_y = y;
			m_leftX = leftX;
			m_rightX = rightX;

			m_line = std::make_shared<sf::RectangleShape>(sf::Vector2f(m_rightX - m_leftX, 41));
			m_line->setPosition(m_leftX, m_y);
			//m_line->setFillColor(sf::Color::Red);

			m_lineTexture.loadFromFile("assets\\shelfmini.png");

			m_line->setTexture(&m_lineTexture);
		}
		~Obstacle() {}

		std::shared_ptr<sf::RectangleShape> Get()
		{
			return m_line;
		}

		int leftX() { return m_leftX; }
		int rightX() { return m_rightX; }
		int Y() { return m_y; }

	private:
		int m_y;
		int m_leftX, m_rightX;

	private:
		std::shared_ptr<sf::RectangleShape> m_line;
		sf::Texture m_lineTexture;
		

	};

}
