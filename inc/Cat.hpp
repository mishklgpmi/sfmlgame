#pragma once
#include "Pet.hpp"

namespace myGame
{

	class Cat : public Pet
	{
	public:
		Cat(int x, int y);
		virtual ~Cat();
		virtual bool LoadTextures();
		virtual bool LoadSounds();
	};

}
