#pragma once
#include "Pet.hpp"
#include "PetFactory.hpp"
#include "Obstacle.hpp"
#include "Object.hpp"
#include "DestroedObjects.hpp"
#include <memory>
#include <thread>

namespace myGame
{
	class Game
	{
	public:
        Game(std::shared_ptr<sf::RenderWindow> window);
        ~Game();

        bool setMusic();
        bool setBackground();
        bool setInfo()
        {
            if (!m_scoreFont.loadFromFile("assets\\arial.ttf"))
            {
                std::cerr << "Error while loading font" << std::endl;
                return false;
            }

            // select the font
            m_scoreText.setFont(m_scoreFont); // font is a sf::Font

            // set the string to display
            m_scoreText.setString(std::to_string(m_score));

            // set the character size
            m_scoreText.setCharacterSize(36); // in pixels, not points!

            // set the color
            m_scoreText.setFillColor(sf::Color::Red);

            // set the text style
            m_scoreText.setStyle(sf::Text::Bold);

            m_scoreText.setPosition(m_width - 100, 40);

            return true;
        }
        bool setMainHero(std::string name);
        bool setObstacles()
        {
            std::shared_ptr<Obstacle> obstacle = std::make_shared<Obstacle>(800, 0, m_width);
            m_obstacles.push_back(obstacle);
            obstacle = std::make_shared<Obstacle>(200, 200, 600);
            m_obstacles.push_back(obstacle);
            obstacle = std::make_shared<Obstacle>(750, 400, 600);
            m_obstacles.push_back(obstacle);
            obstacle = std::make_shared<Obstacle>(650, 600, 900);
            m_obstacles.push_back(obstacle);
            obstacle = std::make_shared<Obstacle>(550, 100, 500);
            m_obstacles.push_back(obstacle);
            return true;
        }
        bool setObjects()
        {
            std::shared_ptr<Object> object = std::make_shared<Object>(650, 600);
            m_objects.push_back(object);
            object = std::make_shared<Object>(500, 700);
            m_objects.push_back(object);
            object = std::make_shared<Object>(300, 500);
            m_objects.push_back(object);
            object = std::make_shared<Object>(m_width - 200, 750);
            m_objects.push_back(object);
            return true;
        }
        bool setDestroedObjects()
        {
            std::shared_ptr<DestroyedObjects> object = std::make_shared<DestroyedObjects>(800, 600);
            m_destroyedObjects.push_back(object);
            object = std::make_shared<DestroyedObjects>(200, 400);
            m_destroyedObjects.push_back(object);
            return true;
        }
        bool Setup();
        bool Resume()
        {
            m_music.play();
            return true;
        }
        void LifeCycle();

        void Gravity()
        {
            for (auto& object : m_destroyedObjects)
            {
                object->dY(10);

                bool onTheFloor = false;
                int oX = object->X();
                int oY = object->Y();
                //for (auto& obstacle : m_obstacles)
                for(int i=0;i<m_obstacles.size();i++)
                {
                    const auto& obstacle = m_obstacles[i];

                    if (oX >= obstacle->leftX() && oX <= obstacle->rightX()
                        && oY > obstacle->Y() && abs(oY - obstacle->Y()) < 20)
                    {
                        object->Y(obstacle->Y());
                        onTheFloor = true;

                        if (i == 0)
                            object->Explosion();
                    }
                }

                if(!onTheFloor)
                    object->Rotate();

            }
        }

	private:
        int m_width, m_height;
        int m_score = 0;
        std::shared_ptr<myGame::Pet> m_pet = nullptr;
        std::vector<std::shared_ptr<Obstacle>> m_obstacles;
        std::vector<std::shared_ptr<Object>> m_objects;
        std::vector<std::shared_ptr<DestroyedObjects>> m_destroyedObjects;

    private:
        std::shared_ptr<sf::RenderWindow> m_window;
        sf::Music m_music;
        sf::Texture m_background;
        sf::Sprite m_backgroundSprite;
        sf::Text m_scoreText;
        sf::Font m_scoreFont;
	};

}
