#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <Obstacle.hpp>
#include <Object.hpp>
#include <DestroedObjects.hpp>

namespace myGame
{
	enum class Direction
	{
		Left,
		Right
	};

	class Pet
	{
	public:
		Pet(int x, int y);
		~Pet();

		virtual bool LoadTextures() = 0;
		virtual bool LoadSounds() = 0;

		sf::Sprite Get();
		void Say();
		void Move(Direction dir);
		void Jump();
		void UpdateObstacles(std::vector<std::shared_ptr<Obstacle>>& obstacles);
		int UpdateObjects(std::vector<std::shared_ptr<Object>>& objects);

		void UpdateDestroedObjects(std::vector<std::shared_ptr<DestroyedObjects>>& objects)
		{
			for (int i = 0; i < objects.size(); i++)
			{
				auto& object = objects[i];

				int oX = object->X();
				int oY = object->Y();
				int oR = object->R();

				int distR = m_r + oR;
				int distXY = (m_x - oX) * (m_x - oX) + (m_y - oY) * (m_y - oY);

				// ��������� �������
				if (distR * distR > distXY)
				{
					if (m_x < oX)
						objects[i]->dX(50);
					else
						objects[i]->dX(-50);
				}
			}
		}

		// TODO
		int X()
		{
			return m_x + m_spritePet.getTexture()->getSize().x / 2;
		}

		int Y()
		{
			return m_y + m_spritePet.getTexture()->getSize().y / 2;
		}

		int R()
		{
			return m_r;
		}

		int shiftY() { return m_spritePet.getTexture()->getSize().y / 2 - 14; }

		bool CanJump() { return m_isOnFloor; }

	protected:
		int m_frameNumber = 0;
		int m_x, m_y, m_r;
		int m_vx = 0, m_vy = 0;
		float m_velocity = 15;
		int m_shift = 0;
		bool m_isOnFloor = true;

	protected:
		// �������
		sf::Sprite m_spritePet;
		sf::Texture m_texturePetRight;
		sf::Texture m_texturePetRightNext;
		sf::Texture m_texturePetLeft;
		sf::Texture m_texturePetLeftNext;
		sf::SoundBuffer m_soundBuffer;
		sf::Sound m_sound;

	};
}
